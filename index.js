//Create main element
let my_main = document.createElement('main');

// Create nav_bar component with logo inside
let nav_bar = document.createElement('navbar');
let logo = document.createElement('logo');
logo.innerText = 'Logo';
nav_bar.appendChild(logo);
my_main.appendChild(nav_bar);

// Create ul element with li items then append it to nav_bar
let nav_menus = document.createElement('ul');
let menu_array = ['Home', 'About', 'Contact', 'Project'];
menu_array.forEach(menu => {
    let menu_item = document.createElement('li');
    menu_item.innerText = menu;
    nav_menus.appendChild(menu_item);
});
nav_bar.appendChild(nav_menus);
nav_menus.classList.add('nav_menu');

// Create hero section with image and append it to main element

// Make an ajax request to unsplash and receive json file.
// let pic_list = [];
const URL = 'https://api.unsplash.com/photos?per_page=8';
const otherParam = {
    headers: {
        Authorization:
            'Client-ID 7d45cf5f8558d3dde060bd23cdf1780800296fb40d30a4e6706c917d4c290565'
    },
    method: 'GET'
};

fetch(URL, otherParam)
    .then(data => {
        return data.json();
    })
    .then(res => (pic_list = res))
    .then(() => {
        pic_list.forEach(pic => {
            let image = document.createElement('img');
            image.setAttribute('src', `${pic.urls.small}`);
            hero.appendChild(image);
        });
    })
    .catch(error => console.log(error));

let hero = document.createElement('div');
hero.classList.add('hero');

my_main.appendChild(hero);

// Create card component
// Create an array of object containing h1 and p, use foreach to iterate through and create card component using each data.

let card_data = [
    {
        h1: 'Testimonial #1',
        p:
            'Lorem ipsum dolor sit amet consectetur adipisicing elit. Unde, aliquam.'
    },
    {
        h1: 'Testimonial #2',
        p: 'Lorem ipsum dolor sit amet consectetur adipisicing elit.'
    },
    {
        h1: 'Testimonial #3',
        p:
            'Unde, aliquam suscipit dolore reiciendis cupiditate ipsum? Tenetur ipsa fuga sit.'
    }
];

let card_list = document.createElement('cardlist');

card_data.forEach(data => {
    let card = document.createElement('card');
    let card_h1 = document.createElement('h1');
    card_h1.innerText = data.h1;
    card.appendChild(card_h1);
    let card_p = document.createElement('p');
    card_p.innerText = data.p;
    card.appendChild(card_p);
    card_list.appendChild(card);
});

my_main.appendChild(card_list);

// Inject elements into main
document.body.appendChild(my_main);
